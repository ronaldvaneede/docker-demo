Build container  
`docker build -t ronaldvaneede/centos-node-hello .`

List images  
`docker images`

Run container  
`docker run -p 49160:8080 -d ronaldvaneede/centos-node-hello`

Get container ID  
`docker ps`

Print app output  
`docker logs ...`

See running containers  
`docker ps`

Test container  
`curl -i localhost:49160`